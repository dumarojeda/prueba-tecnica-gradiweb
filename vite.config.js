import { defineConfig } from 'vite';

export default defineConfig({
  build: {
    outDir: '../dist',
  },
  plugins: [],
  resolve: {
    alias: {
      '~bootstrap': './node_modules/bootstrap',
      '~swiper': './node_modules/swiper',
    },
  },
  root: './src',
});
