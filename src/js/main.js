/**
 * JS using Revealing Module Pattern.
 */
import { products } from './products.js';
import { carousel } from './carousel.js';
import { form } from './form.js';

const App = (() => {
  return {
    init: () => {
      products();
      carousel();
      form();
    },
  };
})();

App.init();
