/**
 * Carousel.
 */
import Swiper from 'swiper';
import { Navigation } from 'swiper/modules';

export const carousel = () => {
  const swiper = new Swiper('.slider_products__swiper', {
    slidesPerView: 'auto',
    spaceBetween: 15,
    cssMode: true,
    mousewheel: true,
    keyboard: true,
    navigation: {
      nextEl: '.slider_products--controls__right',
      prevEl: '.slider_products--controls__left',
    },
    modules: [Navigation],
  });
};
