/**
 * Form.
 */
export const form = () => {
  /* Initializes DOM Constant. */
  const formNewsletter = document.querySelector('form#newsletter');
  const inputEmail = document.querySelector('input#email');
  const divMessages = document.querySelector('div#messages');

  /* Submitting Newsletter Form. */
  formNewsletter.addEventListener('submit', (e) => {
    e.preventDefault();

    if (inputEmail.value.trim() === '') {
      showMessage('error', 'Enter your email.');
      return;
    }

    if (!validateEmail(inputEmail.value)) {
      showMessage('error', 'Enter a valid email.');
      return;
    }

    showMessage('success', 'You have subscribed successfully.');
    inputEmail.value = '';
  });

  /* Email Validation Function. */
  const validateEmail = (email) => {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
  };

  /* Show Messages Function. */
  const showMessage = (type, message) => {
    divMessages.classList.add('active');
    divMessages.classList.add(type);
    divMessages.textContent = message;

    setTimeout(() => {
      divMessages.classList.remove('active');
      divMessages.classList.remove(type);
      divMessages.textContent = '';
    }, 3000);
  };
};
