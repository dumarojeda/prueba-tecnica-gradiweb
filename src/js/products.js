/**
 * Form.
 */
export const products = () => {
  /* Initializes Constant. */
  const apiUrl = 'https://gradistore-spi.herokuapp.com/products/all';
  const carouselContainer = document.querySelector(
    '.slider_products__container'
  );

  /* Load products from the API. */
  const loadProducts = async (e) => {
    try {
      const response = await fetch(apiUrl);
      const data = await response.json();
      const products = data.products.nodes;

      products.forEach((product) => {
        const itemProduct = document.createElement('div');
        itemProduct.classList.add('slider_products__item');
        itemProduct.classList.add('swiper-slide');
        itemProduct.innerHTML = `
                <div class="slider_products__item__image">
                <span class="slider_products__item__image__dto">-20%</span>
                <img src="${product.featuredImage.url}"
                    alt="${product.title}" />
                <a href="#" class="slider_products__item__image__link link white uppercase">See more</a>
                </div>
                <div class="slider_products__item__details">
                <h3 class="slider_products__item__details__name">${
                  product.title
                }</h3>
                <div class="slider_products__item__details__reviews">
                    <div class="slider_products__item__details__reviews__stars ${getReviews(
                      product.tags
                    )}">
                    </div>
                    <div class="slider_products__item__details__reviews__value">(320)</div>
                </div>
                <div class="slider_products__item__details__price">
                    <div class="slider_products__item__details__price__value--strikethrough">
                      ${getCurrencySymbol(product.prices.min.currencyCode)}
                      ${product.prices.min.amount}
                    </div>
                    <div class="slider_products__item__details__price__value">
                      ${getCurrencySymbol(product.prices.max.currencyCode)} 
                      ${product.prices.max.amount}
                    </div>
                </div>
                </div>
              `;

        carouselContainer.appendChild(itemProduct);
      });
    } catch (error) {
      console.error('Error loading products:', error);
    }
  };

  /* Get the currency symbol based on the currency code. */
  const getCurrencySymbol = (currencyCode) => {
    switch (currencyCode) {
      case 'EUR':
        return '€';
      default:
        return '$';
    }
  };

  /* Calculate and return the review stars class. */
  const getReviews = (tags) => {
    const getNumericTag = tags.filter((tag) => !isNaN(tag));
    if (getNumericTag.length === 0) {
      return 'zero';
    }

    const average =
      getNumericTag.reduce((acc, tag) => acc + parseInt(tag), 0) /
      getNumericTag.length;

    let starClass;
    switch (true) {
      case average <= 100:
        starClass = 'one';
        break;
      case average <= 200:
        starClass = 'two';
        break;
      case average <= 300:
        starClass = 'three';
        break;
      case average <= 400:
        starClass = 'four';
        break;
      default:
        starClass = 'five';
        break;
    }

    return starClass;
  };

  /* Load products when the page is loaded. */
  window.addEventListener('load', loadProducts);
};
